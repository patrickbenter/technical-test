namespace ReadyTech.API.Models;

public class WeatherCondition
{
    public double TemperatureCelsius { get; init; }
    public DateTime MeasureTime { get; init; }
}