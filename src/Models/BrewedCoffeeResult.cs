using System.Text.Json.Serialization;

namespace ReadyTech.API.Models;

public class BrewedCoffeeResult
{
    /*
     * Technically the Message property could be separated into a base class so that each request could implement it.
     * However this would couple the response objects to the message property and I don't know enough about the wider system
     * to say whether that kind of coupling would be worth it or just cause problems
     */
    [JsonPropertyName("message")]
    public string Message { get; set; } = string.Empty;
    
    /*
     * PreparedAtTime is returned as a DateTime which is serialised automatically instead of returning a string
     * This is because System.Text.Json already defaults the DateTime serialised format to ISO8601 which we want,
     * but it also means we have more control over the global format of dates we are returning.
     *
     * In future if we need to update the format of all the dates we are returning,
     * we could add a custom DateTime serialiser to Program.cs (builder.Services.AddControllers().AddJsonOptions())
     * and globally alter the returned DateTime string. Otherwise we would potentially need to refactor every endpoint
     * to update the string format.
     */
    [JsonPropertyName("prepared")]
    public DateTime PreparedAtTime { get; init; } 
    // init-only to ensure the prepared time is immutable, we don't want the prepare time of a coffee instance to be able to change later
    // We could also default the value here to DateTime.Now, but I prefer to be explicit and specify it when the object is created
}