using ReadyTech.API.Clients.ResponseModels;
using ReadyTech.API.Models;

namespace ReadyTech.API.Clients;

public class OpenMeteoClient : IWeatherClient
{
    private readonly HttpClient _httpClient;

    public OpenMeteoClient(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }


    public async Task<WeatherCondition> GetCurrentWeatherCondition(double latitude, double longitude)
    {
        var response = await _httpClient.GetAsync($"forecast?latitude={latitude}&longitude={longitude}&current_weather={true}");

        response.EnsureSuccessStatusCode();

        var weatherCondition = await response.Content.ReadFromJsonAsync<OpenMeteoWeatherResponse>();

        if (weatherCondition?.CurrentWeather is null)
        {
            // This could also be a custom exception specific to the OpenMeteo API
            throw new HttpRequestException($"Current weather data not found for latitude {latitude} and longitude {longitude}");
        }

        /*
         * There is some mapping from the external API response to an internal response type
         * While this is kind of mixing concerns, I feel like it's best to happen here in this scenario because it hides the
         * external API implementation details from the rest of the program.
         *
         * In a larger project external service clients could be moved to a separate project
         * where all the external API implementation details are defined internal and inaccessible from the rest of the solution.
         *
         * The reason I've added this mapping is to decouple my controller logic from any one specific API implementation
         */
        return new WeatherCondition()
        {
            MeasureTime = weatherCondition.CurrentWeather.MeasureTime,
            TemperatureCelsius = weatherCondition.CurrentWeather.Temperature
        };
    }
}