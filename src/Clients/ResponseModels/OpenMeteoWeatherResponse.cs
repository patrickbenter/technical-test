using System.Text.Json.Serialization;

namespace ReadyTech.API.Clients.ResponseModels;

/// <summary>
/// Subset of the properties returned from the <a href="https://open-meteo.com/en/docs#api-documentation">OpenMeteo weather API</a>
/// </summary>
public class OpenMeteoWeatherResponse
{
    [JsonPropertyName("elevation")]
    public double Elevation { get; set; }
    [JsonPropertyName("current_weather")]
    public OpenMeteoCurrentWeatherResponse? CurrentWeather { get; set; } 
}

public class OpenMeteoCurrentWeatherResponse
{
    [JsonPropertyName("temperature")]
    public double Temperature { get; set; }
    
    [JsonPropertyName("time")]
    public DateTime MeasureTime { get; set; }
}