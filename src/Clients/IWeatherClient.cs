using ReadyTech.API.Models;

namespace ReadyTech.API.Clients;

public interface IWeatherClient
{
    Task<WeatherCondition> GetCurrentWeatherCondition(double latitude, double longitude);
}