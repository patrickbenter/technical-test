namespace ReadyTech.API;

public static class Constants
{
    public readonly struct ResponseMessages
    {
        public const string HotCoffeeMessage = "Your piping hot coffee is ready";
        public const string ColdCoffeeMessage = "Your refreshing iced coffee is ready";
    }
}
