using Microsoft.AspNetCore.Mvc;
using ReadyTech.API.Clients;
using ReadyTech.API.Extensions;
using ReadyTech.API.Models;
using ReadyTech.API.Services;

namespace ReadyTech.API.Controllers;

[ApiController]
public class CoffeeController : ControllerBase
{
    private readonly IWeatherClient _weatherClient;
    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly ILogger<CoffeeController> _logger;

    /*
     * A static field reused across requests is not a great way of doing this, but it works for a simple solution like this.
     * One limitation of this static field is it is not thread safe so we cannot guarantee that it will trigger every 5 requests in a high volume scenario.
     *
     * If guaranteeing every 5 requests was required, adding a lock would prevent the issue with thread safety. However this is a messy solution and clutters up the endpoint
     * One option in that scenario would be to encapsulate that logic into an injected singleton,
     * however we would need to ensure that the service is not reused elsewhere in case it is modified outside of this class
     *
     * This solution would also provide inconsistent results when scaling horizontally, as each instance of the application would have its own copy of the field.
     * In that case something like Redis would be more appropriate
     */
    private static int _numberOfReceivedRequests = 0;

    public CoffeeController(IWeatherClient weatherClient, IDateTimeProvider dateTimeProvider, ILogger<CoffeeController> logger)
    {
        _weatherClient = weatherClient;
        _dateTimeProvider = dateTimeProvider;
        _logger = logger;
    }

    /// <summary>
    /// Brews a cup of coffee
    /// </summary>
    /// <returns>Whether the coffee was brewed or not</returns>
    [HttpGet]
    [Route("/brew-coffee")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status418ImATeapot)]
    [ProducesResponseType(StatusCodes.Status503ServiceUnavailable)]
    public async Task<ActionResult<BrewedCoffeeResult>> GetBrewedCoffee()
    {
        var timeNow = _dateTimeProvider.Now;
        
        // Assumption: Endpoint should not increment request counter or do any other checks before April 1 check
        // This would be more consistent behaviour so that every request on April 1 returns this response, not every 4 out of 5
        if (timeNow.Month == 4 && timeNow.Day == 1)
        {
            // Coffee is not brewed on the 1st of April
            return StatusCode(StatusCodes.Status418ImATeapot, string.Empty);
        }
        
        _numberOfReceivedRequests++;
        if (_numberOfReceivedRequests.IsMultipleOf(5))
        {
            _logger.LogWarning("Coffee machine has run out of coffee");

            return StatusCode(StatusCodes.Status503ServiceUnavailable, string.Empty);
        }
        
        /*
         * Right now any exceptions are bubbling to the top and are returned in the API response. This is a design decision based on the scope of this assessment.
         * 
         * Catching an exception just to log it is (often) bad design because you lose useful stackframe information in the stacktrace
         * The kind of response we should be returning if an exception occurs would need more clarification
         * on the security constraints vs ease in debugging, the intended API consumer, internal vs external services etc
         *
         * Ideally we would use a global exception handler like this https://learn.microsoft.com/en-us/aspnet/web-api/overview/error-handling/web-api-global-error-handling#custom-error-message-exception-handler
         * which handles all exceptions at the top level and lets us customise error format, fallback message, level of detail etc.
         * This also means we don't need to repeatedly try/catch on the endpoint level and clutter up the controllers with duplicate logic
         */

        // Hardcode Canberra latitude/longitude for the purposes of this demo
        var currentWeather = await _weatherClient.GetCurrentWeatherCondition(latitude: 35.28, longitude: 149.13);

        var response = new BrewedCoffeeResult()
        {
            // Hot coffee message is a saner default option (for example if the return value was the default double value of 0)
            Message = currentWeather.TemperatureCelsius > 30 
                ? Constants.ResponseMessages.ColdCoffeeMessage 
                : Constants.ResponseMessages.HotCoffeeMessage,
            // timeNow is technically very slightly earlier than when this would be executing but I'm assuming this is not important to the consumer
            PreparedAtTime = timeNow
        };
        return Ok(response);
    }
}
