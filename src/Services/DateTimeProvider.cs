namespace ReadyTech.API.Services;

/// <inheritdoc cref="IDateTimeProvider"/>
public class DateTimeProvider : IDateTimeProvider
{
    public DateTime Now  => DateTime.Now;
}