namespace ReadyTech.API.Services;

/// <summary>
/// Wrapper for <see cref="DateTime"/> which when injected allows for easy mocking of DateTime values for unit tests
/// </summary>
public interface IDateTimeProvider
{
    DateTime Now { get; }
}