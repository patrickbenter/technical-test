using Polly;
using Polly.Extensions.Http;
using ReadyTech.API.Clients;

namespace ReadyTech.API.Extensions;

public static class HttpClientExtensions
{
    public static IServiceCollection AddHttpClients(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddHttpClient<IWeatherClient, OpenMeteoClient>(client =>
            {
                // This could go into an appSettings value if needed, say if we were using different endpoints per-environment
                // In this case it's not worth the added configuration
                client.BaseAddress = new Uri("https://api.open-meteo.com/v1/");
            })
            // Add transient fault handling to the HttpClient in case the API is flaky
            .AddPolicyHandler(GetBasicRetryPolicy());
        return serviceCollection;
    }

    /// <summary>
    /// Create a basic retry policy that retries 1 seconds after the first failure, then 4 seconds after the second failure
    /// </summary>
    private static IAsyncPolicy<HttpResponseMessage> GetBasicRetryPolicy()
    {
        return HttpPolicyExtensions
            .HandleTransientHttpError()
            .WaitAndRetryAsync(2, retryCount => TimeSpan.FromSeconds(Math.Pow(retryCount, 2)));
    }
}