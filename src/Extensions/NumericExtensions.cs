namespace ReadyTech.API.Extensions;

public static class NumericExtensions
{
    /// <summary>
    /// More readable way to check whether an integer is a multiple of another
    /// </summary>
    /// <returns>True if `<see cref="number"/>` is a multiple of `<see cref="multiple"/>`</returns>
    public static bool IsMultipleOf(this int number, int multiple)
    {
        return number % multiple == 0;
    }
}