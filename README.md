# Technical Test - Patrick Benter

.NET 6 webapi project using NUnit unit tests

## Part 1 - Brew coffee

Repository branch - https://gitlab.com/patrickbenter/technical-test/-/tree/brew-coffee

Branch history - https://gitlab.com/patrickbenter/technical-test/-/commits/brew-coffee/

## Part 2 - Weather service integration

Repository branch - https://gitlab.com/patrickbenter/technical-test

Branch history - https://gitlab.com/patrickbenter/technical-test/-/commits/weather-service