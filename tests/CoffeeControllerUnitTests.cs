using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using ReadyTech.API;
using ReadyTech.API.Clients;
using ReadyTech.API.Controllers;
using ReadyTech.API.Models;
using ReadyTech.API.Services;

namespace ReadyTech.Tests;

public class CoffeeControllerUnitTests
{
    private CoffeeController _sut = null!;

    private IWeatherClient _weatherClient = null!;
    private IDateTimeProvider _dateTimeProvider = null!;
    private ILogger<CoffeeController> _logger = null!;
    
    private static readonly DateTime DateToday = new DateTime(2023, 1, 1);

    [SetUp]
    public void Setup()
    {
        _weatherClient = Substitute.For<IWeatherClient>();
        _dateTimeProvider = Substitute.For<IDateTimeProvider>();
        _logger = Substitute.For<ILogger<CoffeeController>>();
        
        // Setup default weather conditions
        _weatherClient.GetCurrentWeatherCondition(default, default).ReturnsForAnyArgs(new WeatherCondition()
        {
            TemperatureCelsius = 20,
            MeasureTime = DateToday
        });
        
        // Setup default time for tests which is overriden if required for a test
        _dateTimeProvider.Now.Returns(DateToday);

        _sut = new CoffeeController(_weatherClient, _dateTimeProvider, _logger);
    }

    [Test]
    public async Task BrewCoffee_TemperatureAtMost30Degrees_ShouldReturnHotCoffeeMessage()
    {
        _weatherClient.GetCurrentWeatherCondition(default, default).ReturnsForAnyArgs(new WeatherCondition()
        {
            TemperatureCelsius = 30,
            MeasureTime = DateToday
        });
        
        var response = await _sut.GetBrewedCoffee();

        var actionResult = response.Result as OkObjectResult;
        Assert.That(actionResult, Is.Not.Null);
        Assert.That(actionResult!.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
        Assert.That((actionResult.Value as BrewedCoffeeResult)!.Message, Is.EqualTo(Constants.ResponseMessages.HotCoffeeMessage));
    }

    [Test]
    public async Task BrewCoffee_TemperatureOver30Degrees_ShouldReturnColdCoffeeMessage()
    {
        _weatherClient.GetCurrentWeatherCondition(default, default).ReturnsForAnyArgs(new WeatherCondition()
        {
            TemperatureCelsius = 35,
            MeasureTime = DateToday
        });
        
        var response = await _sut.GetBrewedCoffee();

        var actionResult = response.Result as OkObjectResult;
        Assert.That(actionResult, Is.Not.Null);
        Assert.That(actionResult!.StatusCode, Is.EqualTo(StatusCodes.Status200OK));
        Assert.That((actionResult.Value as BrewedCoffeeResult)!.Message, Is.EqualTo(Constants.ResponseMessages.ColdCoffeeMessage));
    }

    [Test]
    public async Task BrewCoffee_CallEndpointFiveTimes_ShouldReturnServiceUnavailable()
    {
        await _sut.GetBrewedCoffee();
        await _sut.GetBrewedCoffee();
        await _sut.GetBrewedCoffee();
        await _sut.GetBrewedCoffee();
        var fifthCallResponse = await _sut.GetBrewedCoffee();

        var actionResult = fifthCallResponse.Result as ObjectResult;
        Assert.That(actionResult, Is.Not.Null);
        Assert.That(actionResult!.StatusCode, Is.EqualTo(StatusCodes.Status503ServiceUnavailable));
    }

    [Test]
    public async Task BrewCoffee_TodayIsApril1_ShouldReturnImATeapot()
    {
        var aprilFools = new DateTime(2023, 4, 1);
        _dateTimeProvider.Now.Returns(aprilFools);

        var response = await _sut.GetBrewedCoffee();

        var actionResult = response.Result as ObjectResult;
        Assert.That(actionResult, Is.Not.Null);
        Assert.That(actionResult!.StatusCode, Is.EqualTo(StatusCodes.Status418ImATeapot));
    }

    [Test]
    public void BrewCoffee_WeatherClientException_ShouldReturnException()
    {
        var httpException = new HttpRequestException("Call to weather API failed");
        _weatherClient.GetCurrentWeatherCondition(default, default).ThrowsForAnyArgs(httpException);
        
        Assert.ThrowsAsync<HttpRequestException>(async () => await _sut.GetBrewedCoffee());
    }
}